#include <iostream>
#include "bounded_linked_list.h"

constexpr const char nl = '\n';

using SafeBoundedList = ll::bounded_linked_list<ll::default_traits<int, 10>>;

template<class Container>
void print(const Container &c) {
    std::cout << "------------------------------------------------------\n";
    for (auto &v : c) {
        std::cout << v << nl;
    }
    std::cout << "======================================================\n";
}

void test_insert_after() {
    SafeBoundedList bl;
    bl.debug_print(std::cout);
    bl.push_front(42);
    bl.debug_print(std::cout);
    bl.push_front(38);
    bl.debug_print(std::cout);
    bl.pop_front();
    bl.debug_print(std::cout);
    bl.push_front(73);
    bl.push_front(74);
    bl.debug_print(std::cout);
    auto itr = bl.insert_after(bl.cbegin(), 133);
    bl.debug_print(std::cout);
    ++itr;
    ++itr;
    bl.insert_after(itr, 374);
    bl.debug_print(std::cout);
    bl.clear();
    bl.debug_print(std::cout);
}

struct unsafe_traits : public ll::default_traits<int, 10> {
    constexpr static const ll::Safety ListSafety = ll::Safety::UNSAFE;
};

void test_unsafe() {
    ll::bounded_linked_list<unsafe_traits> bl;
    bl.push_front(42);
    bl.pop_front();
    std::cout << "Size of unsafe 10 elem int list: " << sizeof(bl._data) << nl;
    SafeBoundedList sbl;
    std::cout << "Size of safe 10 elem int list: " << sizeof(sbl._data) << nl;
    std::cout << std::boolalpha << "List is empty? " << bl.empty() << nl;
}

void check_sizes() {
    std::cout << "sizeof int " << sizeof(int) << nl;
    std::cout << "sizeof optional int " << sizeof(std::experimental::optional<int>) << nl;
    std::cout << "sizeof int stackobj " << sizeof(balki::stackobj<int>) << nl;
}

int main() {
    std::cout << "Hello, World!" << std::endl;
    check_sizes();
//    return 0;
    test_insert_after();
    test_unsafe();
    SafeBoundedList bl;
    bl.push_front(42);
    auto itr = bl.cbefore_begin();
    bl.insert_after(itr, 38);
    bl.debug_print(std::cout);
    print(bl);
    bl.pop_front();
    for (auto &v : bl) {
        std::cout << v << nl;
    }
    const auto &cbl = bl;
    for (auto &v : cbl) {
        std::cout << v << nl;
    }
    std::cout << std::boolalpha << "List is empty? " << bl.empty() << nl;
    bl.pop_front();
    std::cout << std::boolalpha << "List is empty? " << bl.empty() << nl;
    std::cout << "MaxSize " << bl.max_size() << nl;
    return 0;
}
