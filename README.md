# linkedlist

[WIP] Fixed size, pre-allocated contiguous singly linked list for c++14 or later

## Synopsis

    template<class T, SizeType SIZE>
    struct default_traits : public _traits {
        using ObjectType = T;
        constexpr static const SizeType ListSize = SIZE;
        constexpr static const Safety ListSafety = Safety::SAFE;
    };

    template<class Traits>
    class bounded_linked_list;

## Safety

When list is `Safety::SAFE`, there are no undefined behavior. Any bad action will raise an exception like
1. Incrementing or dereferencing end iterator
2. Accessing using a deleted iterator
3. Comparing iterators of different objects
4. pushing to a full list
5. popping from an empty list

When list is `Safety::UNSAFE`, above actions are undefined behavior. 
1. There is absolutely no overhead to data.

## Installation

TODO: Provide single header
Copy both header files to your project's include path and it should work

For running tests:

    mkdir build
    [sudo] pip install conan
    cd build
    conan install ..
    cmake ..
    make
    ctest 
