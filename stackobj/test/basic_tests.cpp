#include "stackobj.h"
#include "catch2/catch.hpp"

struct S {
    static int num_elements;
    S(){
        num_elements++;
    }
    ~S() {
        num_elements--;
    }
};

int S::num_elements = 0;

TEST_CASE("verify object creation", "[stackobj]") {
    balki::stackobj<int> sbi;
    sbi.emplace(42);
    REQUIRE(*sbi == 42);
}

TEST_CASE("verify constructor", "[stackobj]") {
    balki::stackobj<S> sbs;
    sbs.emplace();
    REQUIRE(S::num_elements == 1);
    SECTION("verify destructor member reset") {
        sbs.reset();
        REQUIRE(S::num_elements == 0);
    }
    SECTION("verify destructor free func reset") {
        reset(sbs);
        REQUIRE(S::num_elements == 0);
    }
}
