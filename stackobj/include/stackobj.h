//
// Created by balki on 1/18/2019.
//

#ifndef LINKEDLIST_STACKOBJ_H
#define LINKEDLIST_STACKOBJ_H

#include <utility>

namespace balki {

    template<class T>
    union stackobj {

        T t;

        stackobj() noexcept {}  // NOLINT(modernize-use-equals-default)

        ~stackobj() noexcept {} // NOLINT(modernize-use-equals-default)

        template <class U>
        void operator=(U&&) = delete;

        stackobj(const stackobj&) = delete;

        stackobj(stackobj&&) = delete;

        template<class... Args>
        void emplace(Args&&... args) {

            new(&t) T{std::forward<Args&&>(args)...};
        }

        void reset() {

            t.~T();
        }

        const T& operator*() const&  noexcept { return t; }

        T& operator*() &  noexcept { return t; }

        const T&& operator*() const&&  noexcept { return std::move(t); }

        T&& operator*() && noexcept { return std::move(t); }
    };

    template<class T>
    void reset(stackobj<T>& opt) {

        opt.reset();
    }
}


#endif //LINKEDLIST_STACKOBJ_H
