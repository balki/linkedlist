conan_basic_setup()
find_package(Catch2 REQUIRED)
add_executable(ltests tests_main.cpp basic_tests.cpp)
target_link_libraries(ltests Catch2::Catch2)
target_link_libraries(ltests linkedlist)

include(Catch)
catch_discover_tests(ltests)
