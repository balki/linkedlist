#include "bounded_linked_list.h"
#include "catch2/catch.hpp"

namespace {
    template<ll::Safety SAFETY>
    struct test_traits : ll::default_traits<int, 10> {
        [[gnu::unused]] constexpr static const ll::Safety ListSafety = SAFETY;
    };

    template<class BLType>
    auto as_vec(BLType &bll) {
        std::vector<typename BLType::value_type> vec;
        for (auto &elem : bll) {
            vec.emplace_back(elem);
        }
        return vec;
    }
}

TEMPLATE_TEST_CASE("Basic common tests", "[bll][template]" // NOLINT(google-explicit-constructor,cert-err58-cpp)
, test_traits<ll::Safety::UNSAFE>, test_traits<ll::Safety::SAFE>) {
    ll::bounded_linked_list<TestType> bll;
    REQUIRE(bll.empty());
    SECTION("Emplacing stuff") {
        bll.emplace_front(49);
        REQUIRE(as_vec(bll) == std::vector<int>{49});
    }
    SECTION("Adding stuff") {
        bll.push_front(42);
        REQUIRE(!bll.empty());
        SECTION("popping stuff") {
            bll.pop_front();
            REQUIRE(bll.empty());
        }
        SECTION("iteration") {
            bll.push_front(32);
            REQUIRE(as_vec(bll) == std::vector<int>{32, 42});
            SECTION("const iteration") {
                const auto &const_bll = bll;
                REQUIRE(as_vec(const_bll) == std::vector<int>{32, 42});
            }
            SECTION("c variant iteration") {
                std::vector<int> vec;
                for (auto const_itr = bll.cbegin(); const_itr != bll.cend(); ++const_itr) {
                    vec.emplace_back(*const_itr);
                }
                REQUIRE(vec == std::vector<int>{32, 42});
            }
        }
        SECTION("insert_after") {
            SECTION("regular iterator") {
                auto itr = bll.begin();
                bll.insert_after(itr, 53);
                REQUIRE(as_vec(bll) == std::vector<int>{42, 53});
            }
            SECTION("regular const iterator") {
                auto itr = bll.cbegin();
                bll.insert_after(itr, 54);
                REQUIRE(as_vec(bll) == std::vector<int>{42, 54});
            }
            SECTION("before begin iterator") {
                auto itr = bll.before_begin();
                bll.insert_after(itr, 54);
                auto c_itr = bll.cbefore_begin();
                bll.insert_after(c_itr, 55);
                REQUIRE(as_vec(bll) == std::vector<int>{55, 54, 42});
            }
        }
        SECTION("iterator checks") {
            auto itr = bll.before_begin();
            ++itr;
            REQUIRE(itr == bll.begin());
            REQUIRE(itr == bll.cbegin());
            ++itr;
            REQUIRE(itr == bll.end());
            REQUIRE(itr == bll.cend());
        }
    }
}

TEST_CASE("Verify default traits", "[silence_ide]") {
    using DT = ll::default_traits<int, 10>;
    REQUIRE(DT::ListSize == 10);
    REQUIRE(DT::ListSafety == ll::Safety::SAFE);
}

