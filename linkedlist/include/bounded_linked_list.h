//
// Created by balki on 1/16/2019.
//

#ifndef LINKEDLIST_BOUNDED_LINKED_LIST_H
#define LINKEDLIST_BOUNDED_LINKED_LIST_H

#include <cstddef>
#include <array>
#include <experimental/optional>
#include <iomanip>
#include "stackobj.h"

#if __cpp_lib_optional
namespace LINKEDLIST_OPTIONAL_NS = std;
#elif __cpp_lib_experimental_optional
namespace LINKEDLIST_OPTIONAL_NS = std::experimental;
#else
#error "Need optional to work"
#endif

namespace ll {

    using SizeType = std::size_t;
    constexpr const SizeType MAX_SIZE = 1024;

    enum class Safety {

        SAFE,
        UNSAFE
    };

    struct bounded_list_error : public std::logic_error {

        using std::logic_error::logic_error;
    };

    template<Safety>
    struct OptionalChooser { };

    template<>
    struct OptionalChooser<Safety::SAFE> {

        template<class... Args>
        using Type = LINKEDLIST_OPTIONAL_NS::optional<Args...>;
    };

    template<>
    struct OptionalChooser<Safety::UNSAFE> {

        template<class... Args>
        using Type = balki::stackobj<Args...>;
    };

    struct _traits {

        template<class Traits>
        using OptionalType = typename OptionalChooser<Traits::ListSafety>::template Type<typename Traits::ObjectType>;
    };

    template<class T, SizeType SIZE>
    struct default_traits : public _traits {

        using ObjectType = T;
        constexpr static const SizeType ListSize = SIZE;
        constexpr static const Safety ListSafety = Safety::SAFE;
    };

    template<class T, SizeType SIZE>
    [[gnu::unused]] constexpr const SizeType default_traits<T, SIZE>::ListSize;

    template<class T, SizeType SIZE>
    [[gnu::unused]] constexpr const Safety default_traits<T, SIZE>::ListSafety;

    template<class Traits>
    class bounded_linked_list;

    template<class T>
    void reset(LINKEDLIST_OPTIONAL_NS::optional<T>& opt) {

        opt = LINKEDLIST_OPTIONAL_NS::nullopt;
    }

    /**
     * Contains helper functions that depend on safety
     */
    template<Safety, class BLTraits>
    struct SafetyHelper {

        using BLType = bounded_linked_list<BLTraits>;
        using ConstIter = typename BLType::const_iterator;

        void static checkNotFull(const BLType&) { }

        void static checkIter(const BLType&, const typename BLType::const_iterator&) { }

        void static checkNotEmpty(const BLType&) { }

        void static check_end(const ConstIter&, const char *) { }

        void static check_index(const ConstIter&) { }

        void static check_cross_object_compare(const ConstIter&, const typename BLType::const_iterator&) { }

        void static destroy(BLType& bl) {

            for(auto itr = bl.begin(); itr != bl.end(); ++itr) {

                reset(bl.getData(itr.index));
            }
        }
    };

    template<class Traits>
    class bounded_linked_list {

        using T = typename Traits::ObjectType;
        using Helper = SafetyHelper<Traits::ListSafety, Traits>;

        constexpr static const SizeType SIZE = Traits::ListSize;
        static_assert(SIZE > 0 && SIZE < MAX_SIZE, "SIZE should be between 0 and MAX_SIZE");

    public:

        using OptionalType = typename Traits::template OptionalType<Traits>;
        using value_type  = T;

        std::array<OptionalType, SIZE> _data{};
        std::array<SizeType, SIZE + 2> _order{};

        struct const_iterator {

            using SelfType = const_iterator;
            using OwnerType = bounded_linked_list;
            const OwnerType *owner;
            SizeType index;

            SelfType& operator++() {

                Helper::check_end(*this, "Attempt to increment end iterator");
                index = owner->_order[index];

                return *this;
            }

            const T& operator*() const {

                Helper::check_end(*this, "Attempt to dereference end iterator");
                Helper::check_index(*this);

                return *owner->getData(index);
            }

            bool operator==(const SelfType& other) const {

                Helper::check_cross_object_compare(*this, other);

                return index == other.index;
            }

            bool operator!=(const SelfType& other) const {

                return !operator==(other);
            }
        };

        struct iterator : public const_iterator {

            using BaseType = const_iterator;

            template<class... Args>
            explicit iterator(Args&&... args):BaseType{std::forward<Args&&>(args)...} { }

            T& operator*() {

                Helper::check_end(*this, "Attempt to dereference end iterator");
                Helper::check_index(*this);

                auto nonConstOwner = const_cast<typename BaseType::OwnerType *>(BaseType::owner);

                return *nonConstOwner->getData(BaseType::index);
            }
        };

        bounded_linked_list() { _reset(); }

        const_iterator before_begin() const { return const_iterator{this, _HEAD_INDEX}; }

        const_iterator cbefore_begin() const { return before_begin(); }

        const_iterator begin() const { return const_iterator{this, _beforeDataHead()}; }

        const_iterator cbegin() const { return begin(); }

        iterator before_begin() noexcept { return iterator{this, _HEAD_INDEX}; }

        iterator begin() noexcept { return iterator{this, _beforeDataHead()}; }

        const_iterator end() const { return const_iterator{nullptr, not_an_index()}; }

        const_iterator cend() const { return end(); }

        iterator end() noexcept { return iterator{nullptr, not_an_index()}; }

        ~bounded_linked_list() { Helper::destroy(*this); }

        bool empty() const { return _beforeDataHead() == not_an_index(); }

        bool full() const { return _beforeFreeListHead() == not_an_index(); }

        void clear() {

            Helper::destroy(*this);
            _reset();
        }

        template<class... Args>
        void emplace_front(Args&&... args) {

            SizeType slot = _createNode(std::forward<Args&&>(args)...);
            _pushIndex(_beforeDataHead(), slot);
        }

        void push_front(T val) { //TODO: rvalue and lvalue overloads

            SizeType slot = _createNode(std::move(val));
            _pushIndex(_beforeDataHead(), slot);
        }

        void pop_front() {

            Helper::checkNotEmpty(*this);

            SizeType slot = _popIndex(_beforeDataHead());
            reset(getData(slot));
            _pushIndex(_beforeFreeListHead(), slot);
        }

        iterator insert_after(const_iterator pos, T val) {

            Helper::checkIter(*this, pos);

            SizeType slot = _createNode(std::move(val));
            _pushIndex(_order[pos.index], slot);

            return iterator{this, slot};
        }

        template<class OS>
        void debug_print(OS& os, int pad = 4) const {
            constexpr const char nl = '\n';
            os << "bounded_linked_list:\n"
                  "MAXSIZE " << max_size() << nl;
            os << "        ";
            for (auto& v : _data) {
                if (v) {
                    os << std::setw(pad) << *v;
                } else {
                    os << std::setw(pad) << "|xx|";
                }
            }
            os << nl;
            for (auto& i : _order) {
                if (i == SIZE + 2) {
                    os << std::setw(pad) << "END";
                } else {
                    os << std::setw(pad) << i;
                }
            }
            os << std::setw(pad) << "|xx|\n";
            for (SizeType i = 0; i < SIZE + 2; ++i) {
                os << std::setw(pad) << i;
            }
            os << std::setw(pad) << "END" << nl;
        }

        constexpr SizeType max_size() const { return SIZE; }

        constexpr static SizeType not_an_index() noexcept { return SIZE + 2; }

        const OptionalType& getData(SizeType index) const { return _data[index - 2]; }

        OptionalType& getData(SizeType index) { return _data[index - 2]; }

    private:

        void _pushIndex(SizeType& index, SizeType newIndex) {

            _order[newIndex] = index;
            index = newIndex;
        }

        template<class... Args>
        SizeType _createNode(Args&&... args) {

            Helper::checkNotFull(*this);

            SizeType slot = _popIndex(_beforeFreeListHead());
            getData(slot).emplace(std::forward<Args&&>(args)...);

            return slot;
        }

        SizeType _popIndex(SizeType& index) const {

            SizeType result = index;
            index = _order[index];

            return result;
        }

        void _reset() {

            _order[0] = not_an_index();

            for (SizeType i = 1; i < SIZE + 1; ++i) {
                _order[i] = i + 1;
            }

            _order[SIZE + 1] = not_an_index();
        }

        // unused attribute to silence ide
        [[gnu::unused]] constexpr static const SizeType _HEAD_INDEX = 0;
        [[gnu::unused]] constexpr static const SizeType _FREE_LIST_HEAD_INDEX = 1;

        SizeType _beforeDataHead() const { return _order[_HEAD_INDEX]; }

        SizeType& _beforeDataHead() { return _order[_HEAD_INDEX]; }

        SizeType _beforeFreeListHead() const { return _order[_FREE_LIST_HEAD_INDEX]; }

        SizeType& _beforeFreeListHead() { return _order[_FREE_LIST_HEAD_INDEX]; }

    };

    template<class Traits>
    constexpr const SizeType bounded_linked_list<Traits>::_HEAD_INDEX;

    template<class Traits>
    constexpr const SizeType bounded_linked_list<Traits>::_FREE_LIST_HEAD_INDEX;

    template<class BLTraits>
    struct SafetyHelper<Safety::SAFE, BLTraits> {
        using BLType = bounded_linked_list<BLTraits>;
        using ConstIter = typename BLType::const_iterator;

        void static checkNotFull(const BLType& bl) {

            if (bl.full()) {

                throw bounded_list_error{"List is full; cannot add items"};
            }
        }

        void static checkIter(const BLType& bl, const typename BLType::const_iterator& itr) {

            if (itr.owner != &bl) {

                throw bounded_list_error{"iterator does not belong to the object"};
            }

            if (itr.index == BLType::not_an_index()) {

                throw bounded_list_error{"Attempt to insert after end"};
            }
        }

        void static checkNotEmpty(const BLType& bl) {

            if (bl.empty()) {

                throw bounded_list_error{"List is empty; cannot remove items"};
            }
        }

        void static check_end(const ConstIter& itr, const char *msg) {

            if (itr.index == BLType::not_an_index()) {

                throw bounded_list_error{msg};
            }
        }

        void static check_index(const ConstIter& itr) {

            if (!itr.owner->getData(itr.index)) {

                throw bounded_list_error{"Attempt to access deleted iterator"};
            }
        }

        void static check_cross_object_compare(const ConstIter& itr, const ConstIter& other) {

            if (itr.owner != other.owner) {

                if (itr.owner && other.owner) {

                    throw bounded_list_error{"Attempt to compare iterators from different containers"};
                }
            }
        }

        void static destroy(BLType& bl) {

            for (typename BLType::OptionalType& elem : bl._data) {

                reset(elem);
            }
        }
    };
}

#endif //LINKEDLIST_BOUNDED_LINKED_LIST_H
